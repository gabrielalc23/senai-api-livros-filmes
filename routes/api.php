<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::prefix('/auth')->controller(AuthController::class)->group(function () {

    Route::post('/login', 'login')->name('login');
    Route::post('/register', 'register')->name('register');
    Route::post('/refresh', 'refresh')->name('refresh');
    Route::post('/logout', 'logout')->name('logout');

});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

